//Подклюбчение модуля галпа
const gulp = require("gulp"),
  sass = require("gulp-sass"),
  autoprefixer = require("gulp-autoprefixer"),
  browserSync = require("browser-sync"),
  del = require("del"),
  concat = require("gulp-concat"),
  terser = require("gulp-terser"),
  htmlmin = require("gulp-htmlmin"),
  cleanCSS = require("gulp-clean-css"),
  imagemin = require("gulp-imagemin"),
  imageminPngquant = require("imagemin-pngquant"),
  imageminJpegRecompress = require("imagemin-jpeg-recompress"),
  sourcemaps = require("gulp-sourcemaps"),
  svgSprite = require("gulp-svg-sprite"),
  svgmin = require("gulp-svgmin"),
  cheerio = require("gulp-cheerio"),
  replace = require("gulp-replace"),
  log = require("fancy-log");

//Порядок подключения css файлов

//Порядок подключения js файлов

function serve() {
  browserSync.init({
    server: {
      browser: "google chrome",
      proxy: "localhost:3001",
      baseDir: "build"
      // injectChanges: true // this is new
    }
  });
}

function clear(done) {
  return del("./build"), done();
}

function build_templates() {
  return gulp
    .src("./src/*.html")
    .pipe(gulp.dest("./build/"))
    .pipe(browserSync.stream());
  //  log('templates changed')
}

function shakaling() {
  return gulp
    .src("./src/img/*.*")
    .pipe(
      imagemin([
        imagemin.optipng({
          optimizationLevel: 5
        })
      ])
    )
    .pipe(gulp.dest("./build/img/"));
}

function svgSpriteBuild() {
  return (
    gulp
    .src("./src/img/svg/*.svg")
    // minify svg
    .pipe(
      svgmin({
        js2svg: {
          pretty: true
        }
      })
    )
    // remove all fill and style declarations in out shapes
    .pipe(
      cheerio({
        run: function ($) {
          $("[fill]").removeAttr("fill");
          $("[stroke]").removeAttr("stroke");
          $("[style]").removeAttr("style");
        },
        parserOptions: {
          xmlMode: true
        }
      })
    )
    // cheerio plugin create unnecessary string '&gt;', so replace it.
    .pipe(replace("&gt;", ">"))
    // build svg sprite
    .pipe(
      svgSprite({
        mode: {
          symbol: {
            sprite: "../sprite.svg",
            example: true
          }
        }
      })
    )

    .pipe(gulp.dest("./build/"))
    .pipe(gulp.dest("./src/"))
  );
}

function img() {
  return gulp.src("./src/img/**/*.*").pipe(gulp.dest("./build/img/"));
}

function watch() {
  gulp.watch("./src/css/**/*.scss", styles, browserSyncReload);
  gulp.watch("./src/js/*.js", scripts, browserSyncReload);
  gulp.watch("./src/*.html", build_templates, browserSyncReload);
  gulp.watch("./src/img/*.*", img);
  gulp.watch("./src/img/svg/*.*", svgSpriteBuild);
  gulp.watch("./src/sprite.svg", svgSpriteBuild);
}

//Таск на стили css
function styles() {
  //Шаблон для поиска файлов CSS
  //Все файлы по шаблону '.src/css/**/ *.css'

  return (
    gulp
      .src("./src/css/main.scss")
      .pipe(sourcemaps.init())
      .pipe(sass().on("error", sass.logError))
      .pipe(autoprefixer("last 2 versions"))
      //Выходная папка для стилей
      .pipe(
        cleanCSS({
          level: 2
        })
      )
      .pipe(sourcemaps.write('.'))
      .pipe(sourcemaps.write())
      .pipe(gulp.dest("./build/css"))
      .pipe(browserSync.stream())
  );
}

function minhtml() {
  return gulp
    .src("./src/*.html")
    .pipe(
      htmlmin({
        collapseWhitespace: true
      })
    )
    .pipe(gulp.dest("./build"));
}

function fonts() {
  return gulp.src("./src/fonts/**/*")
  .pipe(gulp.dest("./build/fonts"));
}

//Таск на скрипты js
function scripts() {
  return gulp
    .src([
      "./src/js/jquery-3.4.1.min.js",
      "./src/js/select2.full.min.js",
      "./src/js/bodyScrollLock.min.js",
      "./src/js/svg-fill.js",
      "./src/js/slick.min.js",
      "./src/js/sprite.js",
      "./src/js/index.js",

    ])
    .pipe(sourcemaps.init())
    .pipe(terser())
    .pipe(concat("main.js"))
    .pipe(sourcemaps.write('.'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest("./build/js/"))
    .pipe(browserSync.stream());
}

function browserSyncReload(done) {
  browserSync.reload();
  done();
}
//Поочередно запустить таски
const build = gulp.series(
  scripts,
  // svgSpriteBuild,
  build_templates,
  svgSpriteBuild,
  styles,
  img,
  minhtml,
  fonts,
  shakaling
);

const develop = gulp.series(clear, build, gulp.parallel(serve, watch));
// Паралельно

//Таск вызывающий функцию styles
exports.styles = styles;
//Таск вызывающий функцию scripts
exports.img = img;
exports.scripts = scripts;
exports.build = build;
exports.html = minhtml;
exports.serve = serve;
exports.images = shakaling;
exports.fonts = fonts;
exports.watch = watch;
exports.build_templates = build_templates;
exports.clear = clear;
exports.develop = develop;
exports.svgSpriteBuild = svgSpriteBuild;
