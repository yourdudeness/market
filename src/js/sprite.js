function loadSprite(file, revision) {
    if (!document.createElementNS || !document.createElementNS('http://www.w3.org/2000/svg', 'svg').createSVGRect) {
        return true;
    }

    var isLocalStorage = 'localStorage' in window && window['localStorage'] !== null,
        data,
        insertIT = function () {
            document.body.insertAdjacentHTML('afterbegin', data);
        },
        insert = function () {
            if (document.body) {
                insertIT();
            } else {
                document.addEventListener('DOMContentLoaded', insertIT);
            }
        };

    revision = revision || Math.random();

    if (isLocalStorage && localStorage.getItem('inlineSVGrev') == revision) {
        data = localStorage.getItem('inlineSVGdata');
        if (data) {
            insert();
            return true;
        }
    }
    $.get(file, function (result, status, request) {
        data = request.responseText.replace('<?xml version="1.0" encoding="utf-8"?>', '')
            .replace(/^<svg/, '<svg style="position: absolute;width: 0;height: 0;left: -100%;"');
        insert();
        if (isLocalStorage) {
            localStorage.setItem('inlineSVGdata', data);
            localStorage.setItem('inlineSVGrev', revision);
        }
    });
}

loadSprite('/sprite.svg');