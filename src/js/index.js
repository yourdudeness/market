$( document ).ready(function() {
  $('.slider-main-init').slick({
    dots: true,
    arrows:false,
    speed: 500,
    fade: true,
  });

  $('.slider-big-size').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: true,
    asNavFor: '.slider-small-size'
  });
  $('.slider-small-size').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: true,
    asNavFor: '.slider-big-size',
    focusOnSelect: true
  });
});


